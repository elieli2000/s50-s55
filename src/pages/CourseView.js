import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {

	const { user } = useContext(UserContext);

	// allows us to gain access to methods that will allow us to redirect a user to a different page after enroling in a course
	const navigate = useNavigate(); // useHistory - old term

	// The "useParams" hook allows us to retreve the courseId passed via URL
	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have Successfully enrolled for this course."
				})

				navigate("/courses");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	};

	useEffect(() => {
		console.log(courseId);

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [courseId])

	return (


		<Container>
			<Row className="my-3">
				<Col lg={{span:6, offset:3}}>
					<Card className="cardHighlight p-3">
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
					    <Card.Text>{description}</Card.Text>
					    <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
					    <Card.Text>{price}</Card.Text>
					    <Card.Subtitle className="mb-2">Class Schedule:</Card.Subtitle>
					    <Card.Text>5:30PM - 9:30PM</Card.Text>
					    {
					    	(user.id !== null)
					    	? <Button className="bg-primary" onClick={() => enroll(courseId)} >Enroll</Button>
					    	: <Button className="bg-primary" as={Link} to="/login" >Login to Enroll</Button>
					    }
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}