import React from 'react';

// Create a Context Object
// A context object as the name suggests is a data type of an object that can be used  to store information that can be shared tto other components within the app.
// Context object is a different approach to passing information between components and it allows us to have easier access by avoiding  the use of prop-drilling.
const UserContext = React.createContext();

// The provider component allows other components to consume/use the context object and supply the nessesary info needed to the context objecct.
export const UserProvider = UserContext.Provider;

export default UserContext;