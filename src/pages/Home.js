// import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

export default function Home() {

	let homeBanner = {
		title: "Zuitt Coding Bootcamp",
		description: "Opportunities for everyone, everywhere.",
		buttonAction: "Enroll now!",
		linkRoute: "/courses"
	}

	return (
		<>
			<Banner bannerProp={homeBanner}/>
			<Highlights/>
			{/*<CourseCard/>*/}
		</>
	)
};