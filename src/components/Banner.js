import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner({bannerProp}) {

	const { title, description, linkRoute, buttonAction } = bannerProp

	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{description}</p>
				<Button as={Link} to={linkRoute} variant="secondary" >{buttonAction}</Button>
			</Col>
		</Row>
	)
}

